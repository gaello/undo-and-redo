﻿using UnityEngine;

/// <summary>
/// Command that moves player in provided direction.
/// </summary>
public class MoveCommand : Command
{
    // Reference to the player movement.
    public PlayerMovement player;

    // Direction value.
    public Vector2Int direction;

    public override void Execute()
    {
        player.MovePlayer(direction);
    }

    public override void Undo()
    {
        player.MovePlayer(direction * -1);
    }

    public override string ToString()
    {
        if (direction == Vector2Int.up)
        {
            return "UP";
        }

        if (direction == Vector2Int.down)
        {
            return "DOWN";
        }

        if (direction == Vector2Int.left)
        {
            return "LEFT";
        }

        if (direction == Vector2Int.right)
        {
            return "RIGHT";
        }

        return base.ToString();
    }
}
