﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

/// <summary>
/// Collecting Command view with events for buttons and showing command list.
/// </summary>
public class UICommandsView : UIView
{
    // Reference to the label with command list.
    [SerializeField]
    private TextMeshProUGUI label;

    // List of command names.
    private List<string> commandList = new List<string>();

    public override void ShowView()
    {
        base.ShowView();

        ShowCommands(new string[0]);
    }

    // Adding new command to the command list label.
    public void ShowCommands(string[] commands)
    {
        commandList.Clear();
        commandList.Add("Command List:");
        foreach (var c in commands)
        {
            commandList.Add(c);
        }
        commandList.Add("End");

        UpdateCommandList(0);
    }

    /// <summary>
    /// Method updates position of the pointer at command list.
    /// </summary>
    /// <param name="commandPointer">Index of current command.</param>
    public void UpdateCommandList(int commandPointer)
    {
        commandPointer++;

        label.text = string.Empty;
        for (int i = 0; i < commandList.Count; i++)
        {
            label.text += string.Format("{0}\t{1}\n", i == commandPointer ? ">" : "", commandList[i]);
        }
    }

    // Event called when Undo Button is clicked.
    public UnityAction OnUndoClicked;

    /// <summary>
    /// Method called by Undo Button.
    /// </summary>
    public void UndoClick()
    {
        OnUndoClicked?.Invoke();
    }

    // Event called when Redo Button is clicked.
    public UnityAction OnRedoClicked;

    /// <summary>
    /// Method called by Redo Button.
    /// </summary>
    public void RedoClick()
    {
        OnRedoClicked?.Invoke();
    }

    // Event called when Up Button is clicked.
    public UnityAction OnUpClicked;

    /// <summary>
    /// Method called by Up Button.
    /// </summary>
    public void UpClick()
    {
        OnUpClicked?.Invoke();
    }

    // Event called when Down Button is clicked.
    public UnityAction OnDownClicked;

    /// <summary>
    /// Method called by Down Button.
    /// </summary>
    public void DownClick()
    {
        OnDownClicked?.Invoke();
    }

    // Event called when Left Button is clicked.
    public UnityAction OnLeftClicked;

    /// <summary>
    /// Method called by Left Button.
    /// </summary>
    public void LeftClick()
    {
        OnLeftClicked?.Invoke();
    }

    // Event called when Right Button is clicked.
    public UnityAction OnRightClicked;

    /// <summary>
    /// Method called by Right Button.
    /// </summary>
    public void RightClick()
    {
        OnRightClicked?.Invoke();
    }
}
