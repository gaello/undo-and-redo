﻿using UnityEngine;

/// <summary>
/// UI root class which holds references to the different views.
/// </summary>
public class UIRoot : MonoBehaviour
{
    [SerializeField]
    private UICommandsView commandsView;
    public UICommandsView CommandsView => commandsView;

    [SerializeField]
    private UIExecutionSummaryView executionSummaryView;
    public UIExecutionSummaryView ExecutionSummaryView => executionSummaryView;
}
