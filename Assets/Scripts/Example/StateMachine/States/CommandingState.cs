﻿using UnityEngine;

/// <summary>
/// Game state in which we are executing / undoing commands.
/// </summary>
public class CommandingState : BaseState
{
    public override void PrepareState()
    {
        base.PrepareState();

        // Attaching events to the UI buttons.
        owner.UI.CommandsView.OnUndoClicked += OnUndoClicked;
        owner.UI.CommandsView.OnRedoClicked += OnRedoClicked;

        owner.UI.CommandsView.OnUpClicked += OnUpClicked;
        owner.UI.CommandsView.OnDownClicked += OnDownClicked;
        owner.UI.CommandsView.OnLeftClicked += OnLeftClicked;
        owner.UI.CommandsView.OnRightClicked += OnRightClicked;

        owner.UI.CommandsView.ShowView();
    }

    public override void DestroyState()
    {
        owner.UI.CommandsView.HideView();

        // Detaching events from the UI buttons.
        owner.UI.CommandsView.OnUndoClicked -= OnUndoClicked;
        owner.UI.CommandsView.OnRedoClicked -= OnRedoClicked;

        owner.UI.CommandsView.OnUpClicked -= OnUpClicked;
        owner.UI.CommandsView.OnDownClicked -= OnDownClicked;
        owner.UI.CommandsView.OnLeftClicked -= OnLeftClicked;
        owner.UI.CommandsView.OnRightClicked -= OnRightClicked;

        base.DestroyState();
    }

    /// <summary>
    /// Method attached to the Redo button.
    /// Executes/Redo current command.
    /// </summary>
    private void OnRedoClicked()
    {
        var idx = owner.CommandInvoker.ExecuteCommand();
        owner.UI.CommandsView.UpdateCommandList(idx);
    }

    /// <summary>
    /// Method attached to the Undo button.
    /// Reverts/Undo current command.
    /// </summary>
    private void OnUndoClicked()
    {
        var idx = owner.CommandInvoker.UndoCommand();
        owner.UI.CommandsView.UpdateCommandList(idx);
    }

    /// <summary>
    /// Method attached to the Up button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnUpClicked()
    {
        var command = new MoveCommand() { direction = Vector2Int.up, player = owner.Player };
        AddCommand(command);
        CheckEndCondition();
    }

    /// <summary>
    /// Method attached to the Down button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnDownClicked()
    {
        var command = new MoveCommand() { direction = Vector2Int.down, player = owner.Player };
        AddCommand(command);
        CheckEndCondition();
    }

    /// <summary>
    /// Method attached to the Left button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnLeftClicked()
    {
        var command = new MoveCommand() { direction = Vector2Int.left, player = owner.Player };
        AddCommand(command);
        CheckEndCondition();
    }

    /// <summary>
    /// Method attached to the Right button.
    /// Adds move command to the buffer and updates command list in the UI.
    /// </summary>
    private void OnRightClicked()
    {
        var command = new MoveCommand() { direction = Vector2Int.right, player = owner.Player };
        AddCommand(command);
        CheckEndCondition();
    }

    /// <summary>
    /// Adds command to the invoker and UI.
    /// </summary>
    /// <param name="command">Command.</param>
    private void AddCommand(Command command)
    {
        var idx = owner.CommandInvoker.ExecuteCommand(command);

        owner.UI.CommandsView.ShowCommands(owner.CommandInvoker.GetCommandNames());
        owner.UI.CommandsView.UpdateCommandList(idx);
    }

    /// <summary>
    /// Checks the end condition.
    /// </summary>
    private void CheckEndCondition()
    {
        // check if player is at the same position as destination.
        if (owner.Player.CurrentPosition == owner.Destination.CurrentPosition)
        {
            var gameOver = new GameOverState();
            gameOver.won = true;
            owner.ChangeState(gameOver);
        }
    }
}

