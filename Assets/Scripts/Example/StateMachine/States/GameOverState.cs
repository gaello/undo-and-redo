﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Game Over state, which just display summary window.
/// </summary>
public class GameOverState : BaseState
{
    public bool won = false;

    public override void PrepareState()
    {
        base.PrepareState();

        owner.UI.ExecutionSummaryView.OnReplayClicked += OnReplayClicked;
        owner.UI.ExecutionSummaryView.ShowMessage(won);

        owner.UI.ExecutionSummaryView.ShowView();
    }

    public override void DestroyState()
    {
        owner.UI.ExecutionSummaryView.HideView();

        owner.UI.ExecutionSummaryView.OnReplayClicked -= OnReplayClicked;

        base.DestroyState();
    }

    /// <summary>
    /// Method attached to the Replay button.
    /// </summary>
    private void OnReplayClicked()
    {
        // Reload current scene.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
