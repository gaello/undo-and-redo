﻿using UnityEngine;

/// <summary>
/// Script attached to the player destination.
/// </summary>
public class PlayerDestination : MonoBehaviour
{
    // Reference to the map.
    [SerializeField]
    private MapGenerator map;

    // Destination position on the map grid.
    private Vector2Int currentPosition;
    public Vector2Int CurrentPosition
    {
        get { return currentPosition; }
        set
        {
            currentPosition = map.ClampTiles(value);
            var newPosition = map.GetTilePosition(currentPosition.x, currentPosition.y);
            transform.localPosition = new Vector3(newPosition.x, 1, newPosition.z) + new Vector3(1f, 0, 1f);
        }
    }
}
